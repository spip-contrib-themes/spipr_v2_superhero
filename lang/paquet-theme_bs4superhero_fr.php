<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bs4superhero
// Langue: fr
// Date: 17-04-2020 15:56:49
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4superhero_description' => 'The brave and the blue',
	'theme_bs4superhero_slogan' => 'The brave and the blue',
);
?>